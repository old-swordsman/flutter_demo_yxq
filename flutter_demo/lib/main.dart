import 'dart:async';

import 'package:flutter/material.dart';
import 'package:win_sea_gong/tools/catch_page.dart';
import 'package:win_sea_gong/tools/global_key.dart';
import 'package:win_sea_gong/win_pages/a_tabbottom/page_tabbar.dart';

void main() {
  realRunApp();
  catchMake();
}

void realRunApp() async {
  runApp(MyApp());
}

/*
  异常 捕捉 ⬇️
*/
catchMake() {
  int first = 0;
  //framework 异常捕获
  FlutterError.onError = (FlutterErrorDetails details) {
    if (first == 0) {
      print('reson---->${details.exception}');
      print('location--->${details.stack.toString()}');
    }
    first++;
  };

  /* runZoned 解开后 部分print打印不输出  有待解决*/
  //dart异常捕获
  runZoned(
    () => runApp(MyApp()),
    zoneSpecification: ZoneSpecification(
      print: (Zone self, ZoneDelegate parent, Zone zone, String line) {
        print('日志--->$line'); // 收集日志
      },
    ),
    onError: (Object obj, StackTrace stack) {
      //var details = makeDetails(obj, stack);
      print('错误信息-->'); //采集错误信息
    },
  );
  //构建异常提示界面 用于替换爆红页面
  ErrorWidget.builder = (FlutterErrorDetails flutterErrorDetails) {
    return CatchPage();
  };
}
//暂时 先注释 后续需要可解开
// FlutterErrorDetails makeDetails(Object obj, StackTrace stack) {
//   // 构建错误信息

//   return stack.toString();
// }
//⬆️

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
      navigatorKey: RouterKey.navigatorState,
      builder: (context, child) {
        child = Scaffold(
          body: GestureDetector(
            onTap: () {
              //键盘隐藏
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus &&
                  currentFocus.focusedChild != null) {
                FocusManager.instance.primaryFocus.unfocus();
              }
            },
            child: child,
          ),
        );
        return child;
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: PageTabbar(),
    );
  }
}
