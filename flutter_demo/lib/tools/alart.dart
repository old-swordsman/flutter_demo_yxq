import 'package:flutter/material.dart';
import 'package:win_sea_gong/tools/global_key.dart';
import 'package:win_sea_gong/tools/show_alart.dart';

class Alart {
  //提示语
  static Future showAlartDialog(String message, int type) async {
    await showDialog(
      barrierColor: Color(0x00000001),
      context: RouterKey.navigatorState.currentState.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return ShowAlart(
          backLart: 1,
          title: message,
          position: type, //0 顶部  1 中部  2 底部
        );
      },
    );
  }
}
