import 'package:flutter/material.dart';

//系统异常  提示页
class CatchPage extends StatefulWidget {
  @override
  _CatchPageState createState() => _CatchPageState();
}

class _CatchPageState extends State<CatchPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(238, 238, 238, 1),
      body: Center(
        child: SizedBox(
            width: MediaQuery.of(context).size.width - 60,
            height: 3 * MediaQuery.of(context).size.height / 4,
            child: Material(
              borderRadius: BorderRadius.circular(10),
              elevation: 24.0,
              color: Color.fromRGBO(238, 238, 238, 1),
              type: MaterialType.card,
              //在这里修改成我们想要显示的widget就行了，外部的属性跟其他Dialog保持一致
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    height:
                        3 * MediaQuery.of(context).size.height / 4 - 65 - 120,
                    child: Center(
                      child: Image.asset(
                        'Assets/icon_tabar/friend.png',
                        width: MediaQuery.of(context).size.width - 60 - 30,
                        height: MediaQuery.of(context).size.width - 60 - 30,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 60,
                    height: 25,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        '系统出错~',
                        style: TextStyle(
                            color: Color.fromRGBO(172, 178, 188, 1),
                            fontSize: 16,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 45,
                  ),
                  Align(
                      alignment: Alignment.center,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Container(
                            width: 60,
                            height: 25,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color: Color.fromRGBO(172, 178, 188, 1),
                                    width: 1)),
                            child: Text(
                              '返回',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(172, 178, 188, 1),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                            )),
                      )),
                  SizedBox(
                    height: 50,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
