import 'package:shared_preferences/shared_preferences.dart';

//同步 处理
class SynchronizePreferences {
  static SharedPreferences preferences;

  static Future<bool> getInstance() async {
    preferences = await SharedPreferences.getInstance();
    return true;
  }

  // ignore: non_constant_identifier_names
  static String Get(String key) {
    return preferences.getString(key);
  }

  // ignore: non_constant_identifier_names
  static List GetList(String key) {
    return preferences.getStringList(key);
  }

  // ignore: non_constant_identifier_names
  static Set(String key, var value) {
    if (value is String) {
      preferences.setString(key, value);
    } else if (value is List) {
      preferences.setStringList(key, value);
    }
  }

  // ignore: non_constant_identifier_names
  static Remove(String key) {
    preferences.remove(key);
  }
}
