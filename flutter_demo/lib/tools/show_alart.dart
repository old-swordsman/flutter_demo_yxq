import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:win_sea_gong/tools/public.dart';

// 纯文字 提示框
class ShowAlart extends StatefulWidget {
  final String title;
  final int backLart; //0 白色， 1 黑色
  final int position; //0 顶部  1 中部  2 底部
  const ShowAlart({Key key, this.title, this.backLart, this.position})
      : super(key: key);
  @override
  _ShowAlartState createState() => _ShowAlartState();
}

class _ShowAlartState extends State<ShowAlart> {
  /// 倒计时的计时器。
  Timer _timer;

  /// 启动倒计时的计时器。
  _startTimer() {
    _timer = Timer(
      // 持续时间参数。
      Duration(seconds: 2),
      // 回调函数参数。
      () {
        Navigator.of(context).pop(true);
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //启动倒计时
    _startTimer();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    //销毁计时器
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return widget.position == 2
        ?
        //底部部
        Center(
            child: Column(
              children: [
                Expanded(
                  child: SizedBox(),
                ),
                SizedBox(
                    //width: 180,
                    //height: 35,
                    child: Material(
                  borderRadius: BorderRadius.circular(10),
                  elevation: 24.0,
                  color: widget.backLart == 1 ? Colors.black54 : Colors.white,
                  type: MaterialType.card,
                  //在这里修改成我们想要显示的widget就行了，外部的属性跟其他Dialog保持一致
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: new Text(
                          widget.title,
                          style: TextStyle(
                              color: widget.backLart == 1
                                  ? Colors.white
                                  : Colors.black),
                        ),
                      ),
                    ],
                  ),
                )),
                SizedBox(
                  height: 90,
                ),
              ],
            ),
          )
        :
        //中部
        Center(
            child: SizedBox(
                //width: 180,
                //height: 35,
                child: Material(
              borderRadius: BorderRadius.circular(10),
              elevation: 24.0,
              color: widget.backLart == 1 ? Colors.black54 : Colors.white,
              type: MaterialType.card,
              //在这里修改成我们想要显示的widget就行了，外部的属性跟其他Dialog保持一致
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: new Text(
                      widget.title,
                      style: TextStyle(
                          color: widget.backLart == 1
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ],
              ),
            )),
          );
  }
}
