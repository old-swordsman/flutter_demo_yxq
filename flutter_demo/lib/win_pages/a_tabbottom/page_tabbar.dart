import 'package:flutter/material.dart';
import 'package:win_sea_gong/win_pages/b_home/pages/page_home.dart';
import 'package:win_sea_gong/win_pages/c_user/pages/page_user.dart';

class PageTabbar extends StatefulWidget {
  //const PageTabbar({ Key? key }) : super(key: key);

  @override
  _PageTabbarState createState() => _PageTabbarState();
}

class _PageTabbarState extends State<PageTabbar> {
  _PageTabbarState();
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    //导航项 显示的页面
    final pages = [
      PageHome(),
      PageUser(),
    ];
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 11,
        unselectedFontSize: 11,
        iconSize: 20,
        items: [
          //首页
          BottomNavigationBarItem(
            icon: currentIndex == 0
                ? Image.asset(
                    'Assets/icon_tabar/friend.png',
                    width: 23,
                    height: 23,
                  )
                : Image.asset(
                    'Assets/icon_tabar/friend.png',
                    width: 23,
                    height: 23,
                  ),
            // ignore: deprecated_member_use
            title: Text("首页",
                style: currentIndex == 0
                    ? TextStyle(
                        color: Color.fromRGBO(39, 153, 93, 1), fontSize: 10)
                    : TextStyle(
                        color: Color.fromRGBO(37, 35, 35, 1), fontSize: 10)),
          ),

          //个人中心
          BottomNavigationBarItem(
            icon: currentIndex == 1
                ? Image.asset(
                    'Assets/icon_tabar/friend.png',
                    width: 23,
                    height: 23,
                  )
                : Image.asset(
                    'Assets/icon_tabar/friend.png',
                    width: 23,
                    height: 23,
                  ),
            // ignore: deprecated_member_use
            title: Text("我的",
                style: currentIndex == 1
                    ? TextStyle(
                        color: Color.fromRGBO(39, 153, 93, 1), fontSize: 10)
                    : TextStyle(
                        color: Color.fromRGBO(37, 35, 35, 1), fontSize: 10)),
          ),
        ],
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          _changePage(index);
        },
      ),
      body: IndexedStack(
        children: pages,
        index: currentIndex,
      ),
    );
  }

  /*切换页面*/
  void _changePage(int index) {
    /*如果点击的导航项不是当前项  切换 */
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
      });
    }
  }
}
