import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:win_sea_gong/tools/public.dart';
import 'package:win_sea_gong/win_pages/b_home/model/model_home.dart';
import 'package:win_sea_gong/win_pages/b_home/provider/provider_home.dart';
import 'package:flutter_app_upgrade/flutter_app_upgrade.dart';

enum Pages {
  PageExmple //导向页面
}

class PageHome extends StatefulWidget {
  //const PageHome({ Key? key }) : super(key: key);

  @override
  _PageHomeState createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  @override
  void initState() {
    super.initState();
  }

  Future<AppUpgradeInfo> _checkAppInfo() async {
    /*
    这里将当前app版本号于服务器返回的版本号做对比，不同就提示升级
    */
    //获取当前app版本号
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    //这里一般访问网络接口，将返回的数据解析成如下格式
    return Future.delayed(Duration(seconds: 1), () {
      return AppUpgradeInfo(
        title: '新版本V1.1.1',
        contents: [
          '1、支持立体声蓝牙耳机，同时改善配对性能',
          '2、提供屏幕虚拟键盘',
          '3、更简洁更流畅，使用起来更快',
          '4、修复一些软件在使用时自动退出bug',
          '5、新增加了分类查看功能'
        ],
        apkDownloadUrl: '',
        force: false,
      );
    });
  }

  _getHomeData() {
    ProviderHome.getHomeRequest(
        param: {'': ''},
        onSuccess: (value) {
          print('成功');
          setState(() {});
        },
        onError: (error) {
          print('失败');
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('首页-功能展示'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () {
              AppUpgrade.appUpgrade(
                context,
                _checkAppInfo(),
                iosAppId: 'id88888888',
              );
            },
            child: Container(
              alignment: Alignment.center,
              width: UISize.width,
              height: 50,
              color: Colors.tealAccent,
              child: Text('检查升级'),
            ),
          ),
        ],
      ),
    );
  }

  //当前页路由跳转
  _goto(Pages page) {
    switch (page) {
      case Pages.PageExmple:
        //  Navigator.push(
        //   context,
        //   MaterialPageRoute(builder: (context) => DeliveryCheckPage()),
        // ).then((value) => _getHomeLoading());
        break;
      default:
    }
  }
}
