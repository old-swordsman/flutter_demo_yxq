import 'package:win_sea_gong/dio/api_utils.dart';
import 'package:win_sea_gong/dio/dio_utils.dart';

class ProviderHome {
  static void getHomeRequest<T>({
    param,
    Function(T) onSuccess,
    Function(String error) onError,
  }) async {
    DioUtils.requestHttp(Apis.home, parameters: param, method: DioUtils.GET,
        onSuccess: (data) {
      print('首页--->$data');
      onSuccess(data);
    }, onError: (error) {
      print('首页错误信息--->$error');
      onError(error);
    });
  }
}
