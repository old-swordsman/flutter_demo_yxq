import 'package:flutter/material.dart';

class PageUser extends StatefulWidget {
  // const Pageuser({ Key? key }) : super(key: key);

  @override
  _PageUserState createState() => _PageUserState();
}

class _PageUserState extends State<PageUser> {
  Future<String> mockNetworkData() async {
    return Future.delayed(Duration(seconds: 2), () => "我是从互联网上获取的数据");
  }

  Map map;
  @override
  void initState() {
    super.initState();
    // print(map['y']);
    //print('123');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 200,
        ),
        Center(
          child: FutureBuilder<String>(
            future: mockNetworkData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              // 请求已结束
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  // 请求失败，显示错误
                  return Text("Error: ${snapshot.error}");
                } else {
                  // 请求成功，显示数据
                  return Text("Contents: ${snapshot.data}");
                }
              } else {
                // 请求未结束，显示loading
                return Container(
                    width: 200,
                    height: 100,
                    color: Colors.red[100],
                    child: Text('loading...')); //CircularProgressIndicator();
              }
            },
          ),
        ),
        Center(
          child: FutureBuilder<String>(
            future: mockNetworkData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              // 请求已结束
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  // 请求失败，显示错误
                  return Text("Error: ${snapshot.error}");
                } else {
                  // 请求成功，显示数据
                  return Text("Contents: ${snapshot.data}");
                }
              } else {
                // 请求未结束，显示loading
                return Text('loading...'); //CircularProgressIndicator();
              }
            },
          ),
        )
      ],
    );
  }
}
