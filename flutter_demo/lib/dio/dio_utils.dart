import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:win_sea_gong/dio/api_utils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:win_sea_gong/tools/alart.dart';
import 'package:win_sea_gong/tools/preferences.dart';
import 'dart:convert' as convert;

class DioUtils {
  static Dio dio;

  /// default options
  static const int CONNECT_TIMEOUT = 300000;
  static const int RECEIVE_TIMEOUT = 150000;
  static const int SEND_TIMEOUT = 200000;

  /// http request methods
  static const String GET = 'get';
  static const String POST = 'post';
  static const String NEWGET = 'newGet';
  static const String PUT = 'put';
  static const String PATCH = 'patch';
  static const String DELETE = 'delete';

  ///判断网络是否可用
  ///0 - none | 1 - mobile | 2 - WIFI
  // ignore: missing_return
  static Future<int> isNetWorkAvailable() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile)
      return 1;
    else if (connectivityResult == ConnectivityResult.wifi)
      return 2;
    else if (connectivityResult == ConnectivityResult.none) return 0;
  }

  /// 创建 dio 实例对象
  static Future<Dio> createInstance() async {
    //dio 初始化定义
    if (dio == null) {
      /// 全局属性：请求前缀、连接超时时间、响应超时时间
      var options = BaseOptions(
          connectTimeout: CONNECT_TIMEOUT,
          receiveTimeout: RECEIVE_TIMEOUT,
          sendTimeout: SEND_TIMEOUT,
          responseType: ResponseType.json,
          validateStatus: (status) {
            // 不使用http状态码判断状态，使用AdapterInterceptor来处理（适用于标准REST风格）
            return true;
          },
          baseUrl: Apis.base,
          headers: {
            'Accept': 'application/json,*/*',
            'Content-Type': 'application/json',
            //'token':''
          });

      dio = new Dio(options);
      //针对https的证书不信任，暂时解决办法
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (client) {
        client.badCertificateCallback = (cert, host, port) {
          return true;
        };
      };
      //网络拦截器
      dio.interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          /*. 拦截器请求 时 相关处理*/
          print('-----------------------当前请求信息----------------------');
          print('----------->baseUrl：${options.baseUrl}');
          print('----------->uri：${options.uri}');
          print('----------->method：${options.method}');
          print('----------->queryParameters：${options.queryParameters}');
          print('----------->receiveTimeout：${options.receiveTimeout}');
          print('----------->connectTimeout：${options.connectTimeout}');
          print('----------->sendTimeout：${options.sendTimeout}');
          print('----------->headers：${options.headers}');
          print('----------------------------------------------------------');
        },
        onResponse: (e) {
          /* 
          拦截器 请求到数据后的 相关处理
          e.data['code'] == 200 sucess
          e.data['code'] == 401 error
          */
          if (e.data['code'] == 200) {
          } else {}
          print('获取数据信息：$e');
        },
        onError: (e) {
          // Navigator.pop(Router.navigatorState.currentState.context);
          // _showErrorDialog('网络连接错误，请重试！')
          if (e.type == DioErrorType.CONNECT_TIMEOUT) {
            print("连接超时");
          } else if (e.type == DioErrorType.SEND_TIMEOUT) {
            print("请求超时");
          } else if (e.type == DioErrorType.RECEIVE_TIMEOUT) {
            print("响应超时");
          } else if (e.type == DioErrorType.RESPONSE) {
            //such as 404 503
            print("出现异常");
          } else if (e.type == DioErrorType.CANCEL) {
            print("请求取消");
          } else {
            print("未知错误");
          }
          print('拦截器错误信息-->：$e');
        },
      ));
    }

    return dio;
  }

  /// 清空 dio 对象
  static clear() {
    dio = null;
  }

  ///Get请求
  static void getHttp<T>(
    String url, {
    parameters,
    Function(T) onSuccess,
    Function(String error) onError,
  }) async {
    try {
      Dio dios = await createInstance();
      Response response;
      response = await dios.get(
        url,
        queryParameters: parameters,
      ); //cancelToken: token
      var responseData = response.data;
      if (responseData['code'] == 0) {
        onSuccess(responseData['data']);
      } else {
        onError(responseData['msg']);
      }

      print('get返回结果-->$responseData');
    } catch (e) {
      print('请求出错：' + e.toString());
      onError(e.toString());
    }
  }

  ///Post请求
  static void postHttp<T>(
    String url, {
    parameters,
    Function(T) onSuccess,
    Function(String error) onError,
  }) async {
    try {
      Dio dios = await createInstance();
      Response response = await dios.post(
        url,
        data: parameters,
      ); //cancelToken: token
      var responseData = response.data;
      var i = responseData['msg'];
      if (responseData['code'] == 0) {
        onSuccess(responseData['data']);
      } else if (responseData['code'] == 200) {
        print('返回结果-->${responseData['code']}');
        onSuccess(responseData['data']);
      } else {
        onError(responseData['msg']);
      }
    } catch (e) {
      print('post返回错误信息 $e');
    }
  }

  /// request Get、Post 请求
//url 请求链接
//parameters 请求参数
//method 请求方式
//onSuccess 成功回调
//onError 失败回调
  static void requestHttp<T>(String url,
      {parameters,
      method,
      Function(T t) onSuccess,
      Function(String error) onError}) async {
    parameters = parameters ?? {};
    method = method ?? 'GET';
    //每次请求前先做网络判断
    if (await isNetWorkAvailable() == 0) {
      Alart.showAlartDialog('当前网络不可用，请检查网络', 1);
    } else {
      if (method == DioUtils.GET) {
        getHttp(
          url,
          parameters: parameters,
          onSuccess: (data) {
            onSuccess(data);
          },
          onError: (error) {
            onError(error);
          },
        );
      } else if (method == DioUtils.NEWGET) {
        // newGetHttp(
        //   url,
        //   parameters: parameters,
        //   onSuccess: (data) {
        //     onSuccess(data);
        //   },
        //   onError: (error) {
        //     onError(error);
        //   },
        // );
      } else if (method == DioUtils.POST) {
        postHttp(
          url,
          parameters: parameters,
          onSuccess: (data) {
            onSuccess(data);
          },
          onError: (error) {
            onError(error);
          },
        );
      } else if (method == DioUtils.PUT) {
        // putHttp(
        //   url,
        //   parameters: parameters,
        //   onSuccess: (data) {
        //     onSuccess(data);
        //   },
        //   onError: (error) {
        //     onError(error);
        //   },
        // );
      } else if (method == DioUtils.DELETE) {
        // deleteHttp(
        //   url,
        //   parameters: parameters,
        //   onSuccess: (data) {
        //     onSuccess(data);
        //   },
        //   onError: (error) {
        //     onError(error);
        //   },
        // );
      }
    }
  }
}
